import csv
from movies_api import models,db


class CSVPopulator:
    def __init__(self, db, csv_file):
        self.db = db
        self.csv_file = csv_file
        self.populate_db()

    def get_csv_entries(self):
        csvfile = open(self.csv_file, newline='')
        reader = csv.DictReader(csvfile, delimiter=';')
        return reader

    def separate_producers(self, producers):
        producers_list = []
        for producer in producers.split(', '):
            joined_producers = producer.split(' and ')
            producers_list += [p.strip() for p in joined_producers]
       
        try:
            producers_list.remove('')
        except:
            return producers_list


    def populate_db(self):
        entries = self.get_csv_entries()

        for row in entries:
            producers_list = self.separate_producers(row['producers'])
            producers_models = models.Producer.to_producers_model_list(
                producers_list
            )

            model = models.Award.query.filter_by(
                title=row['title']
            ).first()

            if model:
                continue

            model = models.Award(
                year=int(row['year']),
                title=row['title'],
                studios=row['studios'],
                producers=producers_models,
                winner=True if row['winner']=='yes' else False
            )

            db.session.add(model)
            db.session.commit()
