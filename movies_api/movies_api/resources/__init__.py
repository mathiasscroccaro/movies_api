from flask_restful import Api

from movies_api.resources.awards import (
    Awards,
    Award
)
from movies_api.resources.producers import Producers

from movies_api import app


api = Api(app)

api.add_resource(Awards, '/awards')
api.add_resource(Award, '/awards/<id>')

api.add_resource(Producers, '/producers')
