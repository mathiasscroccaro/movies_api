from flask import jsonify, make_response
from flask_restful import Resource, reqparse

from movies_api import db
from movies_api import models


class Award(Resource):
    def get(self, id):
        model = models.Award.query.get(id)
        if model is None:
            return make_response(jsonify({
                'message': 'Award id not found',
            }), 404)
        return jsonify(model.serialize())  
    
    def patch(self, id):
        parser = reqparse.RequestParser()
        parser.add_argument(
            'year',
            required=False,
            help="'year' key is required",
            location=['json', 'form']
        )
        parser.add_argument(
            'studios',
            required=False,
            help="'studios' key is required",
            location=['json', 'form']
        )
        parser.add_argument(
            'title',
            required=False,
            help="'title' key is required",
            location=['json', 'form']
        )
        parser.add_argument(
            'producers',
            required=False,
            help="'producers' key is required",
            location=['json', 'form']
        )
        parser.add_argument(
            'winner',
            required=False,
            location=['json', 'form']
        )
        
        args = parser.parse_args()

        year = args.get('year', None)
        studios = args.get('studios', None)
        title = args.get('title', None)
        producers = args.get('producers', None)
        winner = args.get('winner', None)

        model = models.Award.query.get(id)
        if model is None:
            return make_response(jsonify({
                'message': 'Award not found'    
            }), 404)

        if year:
            model.year = year
        if studios:
            model.studios = studios
        if title:
            model.title = title
        if winner:
            model.winner = winner

        db.session.commit()

        return jsonify({
            'message': 'Award updated',
        })
    
    def delete(self, id):
        model = models.Award.query.get(id)
        if model is None:
            return make_response(jsonify({
                'message': 'Award id not found',
            }), 404)
        db.session.delete(model)
        db.session.commit()
        return jsonify({
            'message': 'Award deleted',
        })


class Awards(Resource):
    def get(self):
        return models.Award.all_awards_serialized()
    
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument(
            'year',
            required=True,
            help="'year' key is required",
            location=['json', 'form']
        )
        parser.add_argument(
            'studios',
            required=True,
            help="'studios' key is required",
            location=['json', 'form']
        )
        parser.add_argument(
            'title',
            required=True,
            help="'title' key is required",
            location=['json', 'form']
        )
        parser.add_argument(
            'producers',
            required=True,
            help="'producers' key is required",
            location=['json', 'form'],
            action="append"
        )
        parser.add_argument(
            'winner',
            required=False,
            location=['json', 'form']
        )
        
        args = parser.parse_args()

        year = args.get('year')
        studios = args.get('studios')
        title = args.get('title')
        producers = args.get('producers')
        winner = args.get('winner', False)

        model = models.Award(
            year=year,
            studios=studios,
            title=title,
            winner=bool(winner)
        )

        producers = models.Producer.to_producers_model_list(producers)
        model.producers = producers
        
        db.session.add(model)
        db.session.commit()
        return jsonify({
                'message': 'Award created',
            }
        )
