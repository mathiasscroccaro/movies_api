from flask import jsonify
from flask_restful import Resource

from movies_api import models


class Producers(Resource):
    def get(self):
        return jsonify(
            models.Producer.statistics_from_all()
        )
        
