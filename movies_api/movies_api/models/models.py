from movies_api import app, db


class Award(db.Model):
    __tablename__ = 'awards'

    id = db.Column(db.Integer, primary_key=True)
    year = db.Column(db.Integer)
    title = db.Column(db.String)
    studios = db.Column(db.String)
    producers = db.relationship(
        'Producer', 
        secondary='award_producer', 
        back_populates='awards'
    )
    winner = db.Column(db.Boolean, default=False)

    def __repr__(self):
        return f"<id='{self.id}' year='{self.year}' title='{self.title}' " +\
                f"producers='{self.producers}' winner='{self.winner}'>"

    def serialize(self):
        return {
            'id': self.id,
            'year': self.year,
            'studios': self.studios,
            'title': self.title,
            'producers': [
                producer.name for producer in self.producers
            ],
            'winner': self.winner
        }

    @classmethod
    def all_awards_serialized(cls):
        awards = cls.query.all()
        return {
            'awards': [award.serialize() for award in awards]
        }


class Producer(db.Model):
    __tablename__ = 'producers'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    awards = db.relationship(
        'Award', 
        secondary='award_producer', 
        back_populates='producers',
        order_by=Award.year
    )

    def __repr__(self):
        return self.name

    @classmethod
    def to_producers_model_list(cls, name_list):
        model_list = []
        for name in name_list:
            producer_model = cls.query.filter_by(
                name=name
            ).first()
            if producer_model:
                model_list.append(producer_model)
            else:
                producer_model = cls(name=name)
                db.session.add(producer_model)
                db.session.commit()
                model_list.append(producer_model)
        return model_list

    @classmethod
    def statistics_from_all(cls):
        min_statistics = []
        max_statistics = []
        
        producers = cls.query.all()
        for producer in producers:
            if producer.get_min():
                min_statistics.append(producer.get_min())
            if producer.get_max():
                max_statistics.append(producer.get_max())
        
        return {
            'min': min_statistics,
            'max': max_statistics
        }

    def get_max(self):
        all_awards = self.all_winner_awards_list()
        if len(all_awards) < 2 or not all_awards:
            return None

        all_years = [award.year for award in all_awards]
        all_years.sort()

        next_year_max = 0
        previous_year_max = 0

        delta_year_max = 0
        previuos_year = 0

        for i, current_year in enumerate(all_years):
            if i + 1 == len(all_years):
                break
            else:
                next_year = all_years[i+1]
                if next_year - current_year > delta_year_max:
                    next_year_max = next_year
                    previous_year_max = current_year
                    delta_year_max = next_year - current_year

        return {
            'producer': self.name,
            'interval': next_year_max - previous_year_max,
            'previousWin': previous_year_max,
            'followingWin': next_year_max
        }

    def get_min(self):
        all_awards = self.all_winner_awards_list()
        if len(all_awards) < 2 or not all_awards:
            return None

        all_years = [award.year for award in all_awards]
        all_years.sort()

        next_year_min = 0
        previous_year_min = 0

        delta_year_max = max(all_years) - min(all_years)
        previuos_year = 0

        for i, current_year in enumerate(all_years):
            if i + 1 == len(all_years):
                break
            else:
                next_year = all_years[i+1]
                if next_year - current_year <= delta_year_max:
                    next_year_min = next_year
                    previous_year_min = current_year
                    delta_year_max = next_year - current_year

        return {
            'producer': self.name,
            'interval': next_year_min - previous_year_min,
            'previousWin': previous_year_min,
            'followingWin': next_year_min
        }
    
    def all_winner_awards_list(self):
        winner_awards = []
        for award in self.awards:
            if award.winner:
                winner_awards.append(award)
        return winner_awards

    def all_awards_serialized(self):
        all_winner_awards = self.all_winner_awards_list()
        award_list = []
        for award in all_winner_awards:
            award_list.append({
                    'id': award.id,
                    'year': award.year,
                    'studios': award.studios,
                    'title': award.title,
                    'producers': [
                        producer.name for producer in award.producers
                    ]
                }
            )
        return {
            'winner_awards': award_list
        }


class AwardProducer(db.Model):
    __tablename__ = 'award_producer'

    id = db.Column(db.Integer, primary_key=True)
    award_id = db.Column(db.Integer, db.ForeignKey('awards.id'))
    producer_id = db.Column(db.Integer, db.ForeignKey('producers.id'))
