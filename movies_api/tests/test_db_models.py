from flask import Flask
import unittest

from movies_api import db
from movies_api.models import (
    Award,
    Producer
)


class TestModels(unittest.TestCase):
    def setUp(self):
        app = Flask(__name__)
        
        db.init_app(app)
        app.app_context().push()
        db.create_all()

        self.maxDiff = None

    def tearDown(self):
        app = Flask(__name__)
        db.init_app(app)
        with app.app_context():
            db.drop_all()


    def test_producer_get_max(self):
        expected_result = {
            'producer': 'João',
            'interval': 3,
            'previousWin': 2018,
            'followingWin': 2021
        }

        producer = Producer(
            name='João'    
        )
        
        producer.awards.append(
            Award(year=2018, title='movie_0', winner=True)
        )
        producer.awards.append(
            Award(year=2019, title='movie_1')
        )
        producer.awards.append(
            Award(year=2021, title='movie_2', winner=True)
        )
        producer.awards.append(
            Award(year=2022, title='movie_3')
        )

        db.session.add(producer)
        db.session.commit()

        producer = db.session.query(Producer).first()

        self.assertEqual(producer.get_max(), expected_result)
        self.assertEqual(len(producer.all_winner_awards_list()), 2)
        self.assertEqual(producer.all_awards_serialized(), 
        {
            'winner_awards': [
                {
                    'id': 1,
                    'year': 2018,
                    'studios': None,
                    'title': 'movie_0',
                    'producers': [
                        'João'
                    ]
                },
                {
                    'id': 3,
                    'year': 2021,
                    'studios': None,
                    'title': 'movie_2',
                    'producers': [
                        'João'
                    ]
                }

            ]
        })

        producers_list = Producer.to_producers_model_list(
            ['João', 'Marquinho', 'João', 'Judas']
        )

        self.assertEqual(producers_list[0].id, 1)
        self.assertEqual(producers_list[1].id, 2)
        self.assertEqual(producers_list[2].id, 1)
        self.assertEqual(producers_list[3].id, 3)
