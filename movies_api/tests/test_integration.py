from movies_api import db, app
import unittest


class TestEndpoints(unittest.TestCase):
    def setUp(self):
        db.init_app(app)
        app.config.update({
            "TESTING": True,
            "SQLALCHEMY_DATABASE_URI": "sqlite:///:memory:"
        })
        app.app_context().push()
        db.create_all()

        self.client = app.test_client()

        self.maxDiff = None 

    def tearDown(self):
        db.init_app(app)
        with app.app_context():
            db.drop_all()


    def test_user_story(self):
        # The user wants to see all the awards but there is none in the DB
        self.assertEqual(
            self.client.get('/awards').json, 
            {
                'awards': []
            }
        )

        # The user insert 5 new awards in the DB
        self.client.post(
            '/awards', 
            json={
                'year': 1999,
                'studios': 'Studio 1',
                'producers': ['João', 'Maria'],
                'title': 'Movie 1',
                'winner': True
            })        
        self.client.post(
            '/awards', 
            json={
                'year': 2000,
                'studios': 'Studio 2',
                'producers': ['Maria'],
                'title': 'Movie 2',
                'winner': True
            })
        self.client.post(
            '/awards', 
            json={
                'year': 2001,
                'studios': 'Studio 3',
                'producers': ['João', 'Pafuncio'],
                'title': 'Movie 3',
                'winner': False
            })
        self.client.post(
            '/awards', 
            json={
                'year': 2005,
                'studios': 'Studio 4',
                'producers': ['Gericleuda', 'Pafuncio'],
                'title': 'Movie 4',
                'winner': True
            })
        self.client.post(
            '/awards', 
            json={
                'year': 2010,
                'studios': 'Studio 5',
                'producers': ['João', 'Maria'],
                'title': 'Movie 5',
                'winner': True
            })

        # Then, the user tries to see all the movies
        response = self.client.get('/awards')
        self.assertEqual(response.status_code, 200)
        # and verifies if one of the movies exists, because he is lazy...
        self.assertDictContainsSubset(
            {   
                'id': 5,
                'year': 2010,
                'studios': 'Studio 5',
                'producers': ['João', 'Maria'],
                'title': 'Movie 5',
                'winner': True
            }, 
            response.json['awards'][4]
        )

        # User wants to correct studio name from the last award
        response = self.client.patch(
            '/awards/5',
            json={
                'studios': 'Great Studio!'
            })
        # and check is the name changed
        self.assertEqual(
            self.client.get('/awards/5').json, 
            {  
                'id': 5,
                'year': 2010,
                'studios': 'Great Studio!',
                'producers': ['João', 'Maria'],
                'title': 'Movie 5',
                'winner': True
            }
        )
        # but what he really wanted was to delete it
        self.client.delete('/awards/2'), 
        self.assertEqual(
            self.client.get('/awards/2').json, 
            {
                'message': 'Award id not found',
            }
        )

        # Finally he wants to know some statistics about the producers
        self.assertEqual(
            self.client.get('/producers').json,
            {
                'max': 
                [
                    {
                        'followingWin': 2010, 
                        'interval': 9, 
                        'previousWin': 2001, 
                        'producer': 'João'
                    }, 
                    {
                        'followingWin': 2010, 
                        'interval': 11, 
                        'previousWin': 1999, 
                        'producer': 'Maria'
                    }, 
                    {
                        'followingWin': 2005, 
                        'interval': 4, 
                        'previousWin': 2001, 
                        'producer': 'Pafuncio'
                    }
                ], 
                'min': 
                [
                    {
                        'followingWin': 2001, 
                        'interval': 2, 
                        'previousWin': 1999, 
                        'producer': 'João'
                    }, 
                    {
                        'followingWin': 2010, 
                        'interval': 11, 
                        'previousWin': 1999, 
                        'producer': 'Maria'
                    }, 
                    {
                        'followingWin': 2005, 
                        'interval': 4, 
                        'previousWin': 2001, 
                        'producer': 'Pafuncio'
                    }
                ]
            }
        )
        
    
