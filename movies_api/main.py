from movies_api import app, db
from movies_api import utils


if __name__ == '__main__':
    utils.CSVPopulator(db, 'movielist.csv')
    app.run(debug=True, host='0.0.0.0')
