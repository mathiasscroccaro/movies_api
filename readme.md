# How to test

`sudo docker-compose run --rm movies_api python3 -m unittest`

# How to run

`sudo docker-compose up`

# API description

## GET /awards

**Gets all awards in the DB** 

```
curl -X GET -H "Content-Type: application/json" http://127.0.0.1:5000/awards
```

## POST /awards

**Creates a new award item in the DB**

An example of the JSON body request is shown below. All keys in the example are required, except 'winner'. 

```
curl -X POST -H "Content-Type: application/json" -d '{"year": 1994,"studios": "FOX", "producers": ["João", "Maria"], "title": "Great movie", "winner": true}' http://127.0.0.1:5000/awards
```

## PATCH /awards/[id]

**Updates a field in the <id> entry of the DB**

```
curl -X PATCH -H "Content-Type: application/json" -d '{"studios": "MATHIAS"}' http://127.0.0.1:5000/awards/1
```

## DELETE /awards/[id]

**Deletes the <id> entry of the DB**

```
curl -X DELETE -H "Content-Type: application/json" http://127.0.0.1:5000/awards/1
```

## GET /producers

**Gets the min and max interval between two winner awars of the producers**

```
curl -X GET -H "Content-Type: application/json" http://127.0.0.1:5000/producers
```
